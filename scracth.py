def get_img(OLID, size):
    size = size.upper()
    base_url = "http://covers.openlibrary.org/"
    img_type = 'b'
    if OLID[-1] == 'A':
        img_type = 'a'
    modified_url = base_url + img_type + '/olid/' + OLID + '-' + size + '.jpg'  
