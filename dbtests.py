from unittest import main, TestCase
from models import db, Author, Book, Publisher, Subjects 

class tests(TestCase):

    # --- Database Tests ---

    # Test: - Author - Insert and Remove
    def test_author_db(self):
        test = Author(OLID='example', name='example', bio='example', top_work='example', birth_date="example", death_date='example', num_works=0, website_link='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Author).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Author).filter_by(id = test.id).delete()
        db.session.commit()

    # Test: - Publisher - Insert and Remove
    def test_publisher_db(self):
        test = Publisher(name='example', status='example', website='example', country='example', headquarters='example', founded='example', image_link = 'example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Publisher).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Publisher).filter_by(id = test.id).delete()
        db.session.commit()

    # Test: - Book - Insert and Remove
    def test_book_db(self):
        test = Book(OLID='example', work_id='example', title='example', subtitle='example', page_count=0, description='example', isbn='example', publish_date='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Book).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Book).filter_by(id = test.id).delete()
        db.session.commit()

    # Test: - Subjects - Insert and Remove
    def test_subject_db(self):
        test = Subjects(name='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Subjects).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Subjects).filter_by(id = test.id).delete()
        db.session.commit()

if __name__ == "__main__":
    main()
