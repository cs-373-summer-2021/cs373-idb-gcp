# Scraper setup

## TL;DR (Linux Debian)

Inside the folder ./, copy and paste on command line

```shell
sudo apt update
sudo apt-get install git -y
sudo apt-get install build-essential -y
sudo apt-get install python3.8 -y
sudo apt-get install python3-pip -y
sudo apt-get install python3-venv -y
sudo apt-get install python3-dev -y
sudo apt-get install libpq-dev -y
sudo apt upgrade
git config --global user.email "erik.bari@mac.com"
git config --global user.name "Erik Bari Dos Reis"
```

Exit command line. Then copy and paste on command line

```shell
# This
virtualenv venv
source ./venv/bin/activate
# Or this
python3.8 -m venv venv
source ./venv/bin/activate
```

Then copy and paste

```shell
python3 -m pip install --upgrade pip
pip install --upgrade cython
pip install wheel
pip install -r dev-requirements.txt
```

## Run program

With all setup execute `run.sh`

```shell
(venv) ./> # Run .sh file
(venv) ./> source run.sh
```

## TL;DR (Windows)

Inside the folder .\\, copy and paste on command line

```bat
python -m venv venv
.\venv\Scripts\activate
```

Then copy and paste

```bat
python -m pip install --upgrade pip
pip install --upgrade cython
pip install wheel
pip install -r requirements.txt
```

Then run program

```bat
(venv) .\> REM Run .bat file
(venv) .\> .\run.bat
```

## Create Virtual Environment

Via command line

```bat
.\> python -m venv venv
```

## Activate Virtual Environment

```bat
.\> .\venv\Scripts\activate
(venv) .\> REM We are inside python environment
```

## Deactivate Virtual Environment

```bat
(venv) .\> .\venv\Scripts\deactivate
.\> REM We are outside python environment
```

## Install requirements

With the environment activated, install the requirements.txt

```bat
(venv) .\> REM Update python pip
(venv) .\> python -m pip install --upgrade pip
(venv) .\> REM Install wheel
(venv) .\> REM More info in: https://pip.pypa.io/en/stable/reference/pip_wheel/#description
(venv) .\> pip install wheel
(venv) .\> REM [After successfully installations]
(venv) .\> pip install -r requirements.txt
(venv) .\> REM [Successfully installations]
```

## Run program

With all setup execute `run.bat`

```bat
(venv) .\> REM Run .bat file
(venv) .\> .\run.bat
```
